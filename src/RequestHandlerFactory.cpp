#include "RequestHandlerFactory.hpp"

#include <Poco/URI.h>
#include <Poco/Util/Application.h>

#include <unordered_map>
#include <string>
#include <functional>
#include <algorithm>
#include <regex>

#include "FilesHandlers.hpp"
#include "ServerInfoHandlers.hpp"




namespace {
    using handler = Poco::Net::HTTPRequestHandler;
    using handlerProvider = std::function<handler *()>;
    using handlerRecord = std::pair<std::string, handlerProvider>;
    const std::unordered_map<std::string, handlerProvider> _handlersGet{
        {"/api/files", []{return new FilesListHandler;}},
        {"/api/version", []{return new ApiVersionHandler;}}
    };

    const std::unordered_map<std::string, handlerProvider> _handlersPost{
        // {"/api/test", []{return new TestHandler;}},
        {"/api/files/local", []{return new FilesUploadHandler;}}
    };

    const std::vector<std::pair<std::string, handlerProvider>> _regexpHandlersGet{
        // {"\\/api\\/files\\/.*", []{return new FilesListHandler;}}
    };

    const std::vector<std::pair<std::string, handlerProvider>> _regexpHandlersPost{
        // {"\\/api\\/files\\/.*", []{return new FilesListHandler;}}
    };
}


RequestHandlerFactory::RequestHandlerFactory()
{
}

Poco::Net::HTTPRequestHandler* RequestHandlerFactory::createRequestHandler(
    const Poco::Net::HTTPServerRequest& request
) {
    Poco::URI uri{request.getURI()};
    std::string path = uri.getPath();
    std::string method = request.getMethod();

    Poco::Util::Application& app = Poco::Util::Application::instance();
    app.logger().information(
        "%s (%s) \"%s\"",
        request.clientAddress().toString(),
        method,
        request.getURI()
    );

    if(method == "GET"){
        auto iter = _handlersGet.find(path);
        if(iter != _handlersGet.end()){
            return iter->second();
        }
        auto res = std::find_if(
            _regexpHandlersGet.begin(),
            _regexpHandlersGet.end(),
            [&](const handlerRecord& cmp){
                std::regex regex(cmp.first);
                return std::regex_match(path, regex);
            }
        );
        if(res != std::end(_regexpHandlersGet)){
            return res->second();
        }
    } else if (method == "POST") {
        auto iter = _handlersPost.find(path);
        if(iter != _handlersPost.end()){
            return iter->second();
        }
        auto res = std::find_if(
            _regexpHandlersPost.begin(),
            _regexpHandlersPost.end(),
            [&](const handlerRecord& cmp){
                std::regex regex(cmp.first);
                return std::regex_match(path, regex);
            }
        );
        if(res != std::end(_regexpHandlersPost)){
            return res->second();
        }
    }

    return nullptr;
}
