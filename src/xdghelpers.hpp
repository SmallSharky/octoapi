#pragma once

#include <string>
#include <filesystem>

namespace xdg{
    namespace fs = std::filesystem;
    using path = fs::path;

    /**
     * Get user's home path
     */
    path getHomeDir();

    /**
     * Get user-specific dir for specified purpose.
     * For example, if you want XDG_DATA_HOME, then value
     * of dir should be "DATA" or "data".
     * If there are no way to resolve specified dir,
     * exception will be provided.
     */
    path getHomeDir(const std::string& dir);


    /**
     * Same as getHomeDir, but throws an exception if no dir exists
     */
    path getExistingHomeDir(const std::string& dir);

    /**
     * Get existing user's home path or throw an exception
     */
    path getExistingHomeDir();

    

}
