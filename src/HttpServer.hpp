#pragma once


#include <Poco/Util/ServerApplication.h>
#include <vector>
#include <string>

class HttpServer: public Poco::Util::ServerApplication
{
protected:
    void initialize(Application& self);

    int main(const std::vector<std::string>& args);
};
