
#include "ServerInfoHandlers.hpp"

#include <Poco/Util/Application.h>
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Array.h>
#include <Poco/URI.h>

#include <ostream>
#include <filesystem>
#include <sstream>
#include <vector>
#include <unordered_map>


namespace json = Poco::JSON;

void ApiVersionHandler::handleRequest(
    Poco::Net::HTTPServerRequest& request,
    Poco::Net::HTTPServerResponse& response
) {
    response.setChunkedTransferEncoding(true);
    response.setContentType("application/json");

    json::Object result;

    result.set("api", "0.1");
    result.set("server", "1.3.10");
    result.set("text", "OctoPrint 1.3.10");


    result.stringify(response.send());
}
