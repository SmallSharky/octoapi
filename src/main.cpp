
#include <iostream>
#include <unistd.h>

#include "xdghelpers.hpp"
#include "HttpServer.hpp"




int main(int argc, char ** argv) {
    // Prevent root usage
    uint32_t uid = geteuid();
    if(!uid){
        std::cout<<"You shouldn't run this software as root!"<<std::endl;
        return -1;
    }

    HttpServer srv;
    return srv.run(argc, argv);

    // auto configdir = xdg::getExistingHomeDir("config");
    // std::cout<<"config dir is "<<configdir<<std::endl;
    //
    // auto jopadir = xdg::getExistingHomeDir("jopa");
    // std::cout<<"jopa dir is "<<jopadir<<std::endl;

}
