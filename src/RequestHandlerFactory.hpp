#pragma once

#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerRequest.h>


class RequestHandlerFactory: public Poco::Net::HTTPRequestHandlerFactory
{
public:
    RequestHandlerFactory();
    // {
    // }

    Poco::Net::HTTPRequestHandler* createRequestHandler(
        const Poco::Net::HTTPServerRequest& request
    );
    // {
    //     // if (request.getURI() == "/")
    //     //     return new TimeRequestHandler(_format);
    //     // else
    //     return 0;
    // }
};
