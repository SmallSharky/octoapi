
#include <cstdlib>

#include "xdghelpers.hpp"
#include <stdexcept>
#include <unordered_map>

namespace {
    std::string _env(const std::string& name){
        const char * cstret = getenv(name.c_str());
        if(!cstret) {
            throw std::runtime_error("Env var $" + name + " is not set");
        }
        return {cstret};
    }
}

xdg::path xdg::getHomeDir() {
    return {_env("HOME")};
}

xdg::path xdg::getHomeDir(const std::string& dir){
    std::string upperDir{dir};
    for(auto& c: upperDir) {
        c = std::toupper(c);
    }
    try {
        return _env("XDG_" + upperDir + "_HOME");
    } catch ([[ maybe_unused ]] std::runtime_error& e) {
        path homedir{getHomeDir()};
        static const std::unordered_map<std::string, std::string> _bindings{
            {"DATA", ".local/share"},
            {"CACHE", ".cache"},
            {"CONFIG", ".config"},
            {"STATE", ".local/state"},
            {"BIN", ".local/bin"}
        };
        auto iter{_bindings.find(upperDir)};
        if(iter == _bindings.end()){
            throw std::invalid_argument("Unknown dir " + dir);
        }
        return {homedir/iter->second};
    }
}


xdg::path xdg::getExistingHomeDir(const std::string& dir) {
    auto ret = getHomeDir(dir);
    if(
        (!fs::exists(ret))
        || ret.empty()
    ) {
        throw std::runtime_error(
            "There are no dir \"" + dir + "\" (" + ret.string() + ")"
        );
    }
    return ret;
}

xdg::path xdg::getExistingHomeDir() {
    auto ret = getHomeDir();
    if(
        (!fs::exists(ret))
        || ret.empty()
    ) {
        throw std::runtime_error("There are user dir (" + ret.string() + ")");
    }
    return ret;
}
