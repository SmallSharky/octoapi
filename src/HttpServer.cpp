#include "HttpServer.hpp"

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerParams.h>


namespace poco_net = Poco::Net;

#include "xdghelpers.hpp"
#include "RequestHandlerFactory.hpp"



void HttpServer::initialize(Application& self)
{
    loadConfiguration(
        (xdg::getHomeDir("config")/"octoapi/general.properties").string()
    );
    ServerApplication::initialize(self);
}



int HttpServer::main(const std::vector<std::string>& args)
{
    unsigned short port = static_cast<unsigned short>(
        config().getInt("HttpServer.port", 9876));

    poco_net::ServerSocket svs(port);
    poco_net::HTTPServer srv(
        new RequestHandlerFactory(),
        svs, new poco_net::HTTPServerParams);
    srv.start();
    waitForTerminationRequest();
    srv.stop();
    return Application::EXIT_OK;
}
