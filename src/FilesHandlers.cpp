#include "FilesHandlers.hpp"


#include <Poco/Util/Application.h>
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Array.h>
#include <Poco/URI.h>

#include <Poco/Net/HTMLForm.h>

#include <ostream>
#include <filesystem>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <random>
#include <algorithm>
#include <iterator>
#include "xdghelpers.hpp"

namespace json = Poco::JSON;
namespace fs = std::filesystem;
using Status = Poco::Net::HTTPResponse::HTTPStatus;


namespace{

bool stob(std::string s, bool throw_on_error = true)
{
    auto result = false;    // failure to assert is false

    std::istringstream is(s);
    // first try simple integer conversion
    is >> result;

    if (is.fail())
    {
        // simple integer failed; try boolean
        is.clear();
        is >> std::boolalpha >> result;
    }

    if (is.fail() && throw_on_error)
    {
        throw std::invalid_argument(s.append(" is not convertable to bool"));
    }

    return result;
}

std::unordered_map<std::string, std::string> vecToMap(const std::vector<std::pair<std::string, std::string>>& inp) {
    std::unordered_map<std::string, std::string> ret;
    for(auto& p: inp) {
        ret[p.first] = p.second;
    }
    return ret;
}

void makeError(
    Poco::Net::HTTPServerResponse& response,
    Status status,
    const std::string& description
) {
    json::Object result;
    std::stringstream ss;
    result.set("error", description);
    result.stringify(ss);
    response.setStatusAndReason(status, "Error");
    result.stringify(response.send());
}

fs::path getExistingAbsolutePath(const fs::path& p) {
    fs::path ret{p};
    if(!fs::exists(ret)){
        throw std::runtime_error{"path " + ret.string() + " does not exist"};
    }
    if(fs::is_symlink(ret)) {
        ret = fs::read_symlink(ret);
    }
    if(!fs::exists(ret)) {
        throw std::runtime_error{"path " + ret.string() + " does not exist"};
    }
    return ret;
}

std::string bytesHumanReadable(size_t n) {
    static const std::vector<std::string> units{
        "B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "YiB"
    };
    size_t before = n, after = 0, order = 0;
    while(order+1<units.size() && before>1024){
         ++order;
         after = before%1024;
         before /= 1024;
     }
     after = after*100/1024;
     std::stringstream ss;
     ss<<before;
     if(after) {
         ss<<'.'<<after;
     }
     ss<<" "<<units[order];
     return ss.str();
}


json::Object fileToJson(const fs::path& p, const fs::path& base) {
    json::Object ret;
    // TODO: name should consist of ASCII, display can be in any fmt
    ret.set("name", p.filename().string());
    ret.set("display", p.filename().string());
    auto prox = fs::proximate(p, base);
    ret.set("path", prox.string());
    ret.set("origin", "local");
    ret.set("size", std::to_string(fs::file_size(p)));
    std::string ext;
    if(p.has_extension()){
        ext = p.extension().string().substr(1);
    }
    std::string type;
    if(ext == "gcode" || ext == "gco") {
        type = "machinecode";
    } else {
        type = "model";
    }
    json::Array typePath;
    typePath.add(type);
    if(p.has_extension()){
        typePath.add(ext);
    }
    ret.set("typePath", typePath);
    ret.set("type", type);

    return ret;
}

json::Object pathToJson(const fs::path& p, const fs::path& base, bool recursive = false);

json::Object directoryToJson(const fs::path& p, const fs::path& base, bool recursive = false) {
    json::Object ret;
    ret.set("name", p.filename().string());
    ret.set("display", p.filename().string());
    ret.set("type", "folder");

    auto prox = fs::proximate(p, base);
    ret.set("path", prox.string());

    if(recursive || prox == prox.filename()) {
        std::vector<json::Object> children;
        for(auto& _p: fs::directory_iterator(p)) {
            try {
                children.push_back(pathToJson(_p, base, recursive));
            } catch ([[maybe_unused]]std::runtime_error& e) {}
        }
        ret.set("children", children);
        // TODO: fix
        // ret.set("size", std::to_string(fs::file_size(p)));
    }



    return ret;
}

json::Object pathToJson(const fs::path& p, const fs::path& base, bool recursive) {
    if(fs::is_directory(p)) {
        return directoryToJson(p, base, recursive);
    } else if (fs::is_regular_file(p)) {
        return fileToJson(p, base);
    } else if (fs::is_symlink(p)) {
        return pathToJson(fs::read_symlink(p), base);
    }
    throw std::runtime_error("Unsupported path type for " + p.string());
}

} // namespace


void FilesListHandler::handleRequest(
    Poco::Net::HTTPServerRequest& request,
    Poco::Net::HTTPServerResponse& response
) {
    Poco::Util::Application& app = Poco::Util::Application::instance();


    response.setChunkedTransferEncoding(true);
    response.setContentType("application/json");

    json::Object result;
    std::vector<json::Object> files;

    fs::path filesPath;
    try{
        filesPath = getExistingAbsolutePath(
            xdg::getHomeDir("data")/"octoapi/uploads"
        );
    } catch ([[maybe_unused]] std::runtime_error& e) {
        makeError(
            response,
            Status::HTTP_INTERNAL_SERVER_ERROR,
            "Uploads dir does not exist"
        );
        return;
    }

    Poco::URI uri{request.getURI()};
    auto params = vecToMap(uri.getQueryParameters());
    bool recursive = params.count("recursive")?stob(params["recursive"]):false;

    for(auto& p: fs::directory_iterator(filesPath)) {
        try {
            files.push_back(pathToJson(p, filesPath, recursive));
        } catch (std::runtime_error& e) {
            app.logger().information("%s", e.what());
        }
    }

    result.set("files", files);

    auto space = fs::space(filesPath);
    result.set("free", bytesHumanReadable(space.available));

    result.stringify(response.send());
}

#include <Poco/Net/PartHandler.h>
#include <Poco/Net/MessageHeader.h>

class FileUploader: public Poco::Net::PartHandler {
public:
    FileUploader(Poco::Net::HTTPServerRequest& request, const fs::path& path):
        _request(request),
        _path(path)
    {

    }

    void upload(){
        // Poco::URI uri{_request.getURI()};
        Poco::Net::HTMLForm form;
        form.setFieldLimit(100*1024*1024);
        auto& request = _request.get();
        form.load(request, request.stream(), *this);
        fs::path relativeLocation;
        if(form.has("path")){
            relativeLocation = form["path"];
            fs::path tmp = _path;
            for(auto dir: relativeLocation) {
                tmp /= dir;
                if(!fs::exists(tmp)){
                    fs::create_directory(tmp);
                }
            }
        }


        for(auto& record: _fileRecords) {
            fs::path relativeFilename{relativeLocation/record.filename};
            std::cout<<"moving:"<<record.path<<" > "<<relativeFilename<<std::endl;
            fs::rename(record.path, fs::path(_path/relativeFilename));
            std::cout<<"uploaded:"<<relativeFilename<<std::endl;
        }
    }
protected:
    std::reference_wrapper<Poco::Net::HTTPServerRequest> _request;
    fs::path _path;
    struct FileRecord{
        fs::path path;
        std::string filename;
    };
    std::vector<FileRecord> _fileRecords;

    std::string _getFilenameFromDisposition(const std::string& disposition) {
        // TODO: maybe we should check dispositionValue
        // correctness or something else
        std::string dispositionValue;
        Poco::Net::NameValueCollection collection;
        Poco::Net::MessageHeader::splitParameters(
            disposition,
            dispositionValue,
            collection
        );
        return collection["filename"];
    }

    virtual void handlePart(
        const Poco::Net::MessageHeader & header,
        std::istream & stream
    ){
        if(header["Content-Type"]!="application/octet-stream"){
            throw std::invalid_argument(
                "Invalid content type ("
                + header["Content-Type"]
                + ")"
            );
        }
        std::string filename = _getFilenameFromDisposition(
            header["Content-Disposition"]
        );

        fs::path p;
        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
        std::uniform_int_distribution<size_t> distrib(10000000, 99999999);
        size_t i, i_max = 10;
        for(i = 0; i<i_max; ++i) {
            size_t n = distrib(gen);
            p = _path / fs::path(std::to_string(n) + ".tmp");
            if(!fs::exists(p)){
                break;
            }
        }
        if(i==i_max){
            std::cout<<"skipping due to random error"<<std::endl;
            return;
        }
        std::cout<<"accepting:"<<p<<" | "<<filename<<std::endl;
        std::ofstream of(p);
        if(!of.good()){
            std::cout<<"cannot open file "<<p<<std::endl;
            return;
        }
	std::copy(
	    std::istreambuf_iterator<char>(stream),
	    std::istreambuf_iterator<char>( ),
	    std::ostreambuf_iterator<char>(of)
	);
        _fileRecords.push_back({
            .path = p,
            .filename = filename
        });


        // сгенерить имя файла вида "<число>.tmp"
        // убедиться, что такого файла нет, иначе попробовать снова
        // Если после 10-й попытки не получилось, нужно прекратить попытки.
        // Создать файл и записать туда содержимое stream
        // for(auto& h: header) {
        //     std::cout<<h.first<<":"<<h.second<<std::endl;
        // }

        // while(!stream.eof()) {
        //     std::cout<<static_cast<char>(stream.get());
        // }

    }
};

void FilesUploadHandler::handleRequest(
    Poco::Net::HTTPServerRequest& request,
    Poco::Net::HTTPServerResponse& response
) {
    response.setChunkedTransferEncoding(true);
    response.setContentType("application/json");

    json::Object result;
    // std::vector<json::Object> files;

    fs::path filesPath;
    try{
        filesPath = getExistingAbsolutePath(
            xdg::getHomeDir("data")/"octoapi/uploads"
        );
    } catch ([[maybe_unused]] std::runtime_error& e) {
        makeError(
            response,
            Status::HTTP_INTERNAL_SERVER_ERROR,
            "Uploads dir does not exist"
        );
        return;
    }

    Poco::Util::Application& app = Poco::Util::Application::instance();
    // app.logger().information(
    //     "Uploading"
    // );

    FileUploader fu{request, filesPath};
    fu.upload();

    // TODO:
    /*
    Через form проходят значения print (bool) и path (string)
    Через PartHandler проходят заголовки:
        Content-Disposition (form-data; name="file"; filename="<имя файла>")
        Content-Type (application/octet-stream)
    Еще через PartHandler проходит сам файл.
    */

    result.stringify(response.send());


}
